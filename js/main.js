import 'bootstrap';
import { data } from 'jquery';

$(function() {
    $(document).ready(function() {
        $("body").tooltip({ selector: '[data-toggle=tooltip]' });
    });

    $('.media').media();
});
